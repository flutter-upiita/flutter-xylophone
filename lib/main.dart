import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                playSound('note1.wav', Colors.red),
                playSound('note2.wav', Colors.orange),
                playSound('note3.wav', Colors.yellow),
                playSound('note4.wav', Colors.green),
                playSound('note5.wav', Colors.blue),
                playSound('note6.wav', Colors.teal),
                playSound('note7.wav', Colors.purple),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget playSound(String _sound, Color _color) {
  return Expanded(
    child: FlatButton(
      color: _color,
      onPressed: () {
        final player = AudioCache();
        player.play(_sound);
      },
    ),
  );
}
